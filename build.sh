#!/bin/bash
set -euo pipefail # Debugs errors
set +u # Catches unbound variables

read -p "Commit message: " commitmsg

x86_pkgbuild=$(find pkgbuild/ -type f -name "*.pkg.tar.*")

for x in ${x86_pkgbuild}
do
    cp "${x}" x86_64/
    echo "==> Copying ${x}."
done

## Arch: x86_64
cd x86_64
if [[ ! $1 == "--doc" ]]
then
    rm -f tomkoid-repo*
fi

echo "###################################"
echo "Building for architecture 'x86_64'."
echo "###################################"

if [[ $1 == "--doc" ]]
then
    echo "==> Skipping."
else
    ## repo-add
    ## -s: signs the packages
    ## -n: only add new packages not already in database
    ## -R: remove old package files when updating their entry
    repo-add -R -n -s -k EA9C146B97A5211B03184A71D4403E79602EE85B tomkoid-repo.db.tar.gz *.pkg.tar.zst *.pkg.tar.xz

    # Removing the symlinks because GitLab can't handle them.
    rm tomkoid-repo.db
    rm tomkoid-repo.db.sig
    rm tomkoid-repo.files
    rm tomkoid-repo.files.sig

    # Renaming the tar.gz files without the extension.
    mv tomkoid-repo.db.tar.gz tomkoid-repo.db
    mv tomkoid-repo.db.tar.gz.sig tomkoid-repo-db.sig
    mv tomkoid-repo.files.tar.gz tomkoid-repo.files
    mv tomkoid-repo.files.tar.gz.sig tomkoid-repo.files.sig
fi

git add ../addrepo.sh ../README.md ../x86_64 ../LICENSE ../keys ../images

git commit -S -am "$commitmsg"
git push git@codeberg.org:Tomkoid/tomkoid-repo.git main

echo "#######################################"
echo "Packages in the repo have been updated!"
echo "#######################################"
