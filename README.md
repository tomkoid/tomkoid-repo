<img src="images/tomkoid-repo.png">
<p></p>

### This repo is for pacman (Arch Linux package manager). In this repo you will find my scripts and programs.

---

<p></p>

<img src="images/tomkoid-repo-install.png">

<p></p>

### <b><u>1. Run script</u></b>
To add tomkoid-repo, you need to paste this command to terminal:
```
curl https://gitlab.com/Tomkoid/tomkoid-repo/-/raw/main/addrepo.sh | sh
```
This command will add repo to pacman.conf and import keyring.

### <b><u>2. Sync repositories</u></b>
```
sudo pacman -Syyu
```

### 3. Try to download some script
You can install some script now, but we will try to install something simple now. There is `hello-world` package in this repo, so run `sudo pacman -S hello-world` and `helloworld` in terminal and it should output `Hello, World!`

---

<p></p>

<img src="images/tomkoid-repo-software-list.png">

<p></p>

- **blokator** => Simple cross-platform and system-wide CLI adblocker
- **printer-fix** => Installs CUPS and other additional software for printers
- **bluetooh-fix** => Installs bluez and blueman
- **blurry-text-fix** => This script is for GTK4 / GNOME apps, that fixes text blur on small screen resolutions.
- **allow-old-extensions** => This script is for GNOME, that allows to install outdated extensions (be careful when installing big extensions like Dash to Dock)
- **emoji-pack** => Installs some emoji fonts
- **ttf-ms-fonts** => Package of Microsoft fonts
- **cpr** => CPR is C++ library for HTTP requests
- **hello-world** => Testing package
- **fancy-tasker**
- **tomkoid-utils** => Extra or coreutil commands, that are rewritten in Rust
